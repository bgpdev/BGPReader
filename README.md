# README

## Introduction
The BGPReader project contains a BGPReader class written in Java that can read BGP messages from an InputStream.

## Example usage:
Sometimes an InputStream does not contain the BGP capability negotiation messages prior to exchanging BGP Updates.
This is why it is necessary to set the capabilities manually sometimes.
```
// Create a Settings object for the BGPReader
BGPReader.Settings settings = new BGPReader.Settings();
settings.EXTENDED_AS4 = true;
```

To create a BGPReader just pass in the InputStream and a Settings object:
```
// Create a BGPReader that reads BGPMessages from a stream.
InputStream stream = ...
BGPReader reader = new BGPReader(stream, settings);
```

To read a BGPMessage from the stream:
```
// Reads the next BGPMessage.
BGPMessage x = reader.read();

if (x instanceof BGPUpdate)
   ...
if (x instanceof BGPOpen)
   ...
if (x instanceof BGPNotification)
   ...
if (x instanceof BGPKeepAlive)
   ...
```