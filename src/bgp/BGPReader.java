package bgp;

import java.io.DataInputStream;
import java.io.EOFException;
import java.io.IOException;
import java.util.Date;

public class BGPReader
{
    private DataInputStream stream;
    private Settings settings;

    public static class Settings
    {
        public boolean EXTENDED_AS4;
        public boolean MRTMode;
    }

    public BGPReader(DataInputStream stream, Settings settings) throws Exception
    {
        this.stream = stream;
        this.settings = settings;
    }

    public BGPMessage read() throws Exception
    {
        // Parse the 19-byte BGPHeader.
        BGPHeader header = new BGPHeader(stream);

        try
        {
            switch(BGPMessage.Type.fromInt(header.getType()))
            {
                case Open:
                    // TODO: This should be parsed into a class.
                    stream.read(new byte[header.getLength() - 19]);
                    return new BGPOpen();

                case Update:
                    return new BGPUpdate(stream, header, settings);

                case Notification:
                    // TODO: This should be parsed into a class.
                    stream.read(new byte[header.getLength() - 19]);
                    return new BGPNotification();

                case KeepAlive:
                    return new BGPKeepAlive();

                default:
                    throw new Exception("Unknown BGPHeader Type Found.");
            }
        }
        catch(EOFException e)
        {
            throw new Exception("[" + new Date().toString() + "] BGPReader Error!");
        }
    }

    public void close() throws IOException
    {
        stream.close();
    }
}
