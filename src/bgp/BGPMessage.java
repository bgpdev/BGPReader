package bgp;

public abstract class BGPMessage
{
    public enum Type
    {
        Open,
        Update,
        Notification,
        KeepAlive;

        public static Type fromInt(int x) throws Exception
        {
            switch(x)
            {
                case 0x01:
                    return Open;
                case 0x02:
                    return Update;
                case 0x03:
                    return Notification;
                case 0x04:
                    return KeepAlive;
                default:
                    throw new Exception("Invalid parameter: " + x + " is not a BGPMessage Type.");
            }
        }
    }

    public abstract void print();
}
