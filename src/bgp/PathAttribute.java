package bgp;

import bgp.attributes.*;

import java.io.DataInputStream;
import java.io.IOException;

public abstract class PathAttribute
{
    public final AttributeFlags flags;

    protected PathAttribute(AttributeFlags flags)
    {
        this.flags = flags;
    }

    public static PathAttribute fromStream(DataInputStream stream, BGPReader.Settings settings) throws Exception
    {
        AttributeFlags flags = new AttributeFlags(stream.readByte());

        int type = stream.readUnsignedByte();
        switch(type)
        {
            case 1:
                return new ORIGIN(flags, stream);
            case 2:
                return new AS_PATH(flags, stream, settings);
            case 3:
                return new NEXT_HOP(flags, stream);
            case 4:
                return new MULTI_EXIT_DISC(flags, stream);
            case 5:
                return new LOCAL_PREF(flags, stream);
            case 6:
                return new ATOMIC_AGGREGATE(flags, stream);
            case 7:
                return new AGGREGATOR(flags, stream, settings);
            case 8:
                return new COMMUNITY(flags, stream);
            case 12:
                return new ADVERTISER(flags, stream);
            case 14:
                return new MP_REACH_NLRI(flags, stream, settings);
            case 15:
                return new MP_UNREACH_NLRI(flags, stream);
            case 16:
                return new EXTENDED_COMMUNITY(flags, stream);
            case 17:
                return new AS4_PATH(flags, stream, settings);
            case 18:
                return new AS4_AGGREGATOR(flags, stream, settings);
            case 20:
                return new CONNECTOR(flags, stream);
            case 21:
                return new AS_PATHLIMIT(flags, stream);
            case 32:
                return new LARGE_COMMUNITY(flags, stream);
            case 128:
                return new ATTR_SET(flags, stream);
            default:
                throw new IOException("UNRECOGNIZED: " + type);
        }
    }

    public abstract void print();
}
