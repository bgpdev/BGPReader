package bgp;

import network.entities.Prefix;

import java.io.DataInputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class BGPUpdate extends BGPMessage implements Serializable
{
    public ArrayList<Prefix> withdrawnRoutes = new ArrayList<>();
    public Map<String, PathAttribute> pathattributes = new HashMap<>();
    public ArrayList<Prefix> nlri = new ArrayList<>();

    private final static int MIN_UPDATE_SIZE = 23;

    public BGPUpdate(DataInputStream stream, BGPHeader header, BGPReader.Settings settings) throws Exception
    {
        // Parse the Withdrawn Routes section.
        int withdrawnRoutesLength = stream.readUnsignedShort();
        int counter = stream.available();
        while(withdrawnRoutesLength > counter - stream.available())
            withdrawnRoutes.add(new Prefix(stream, Prefix.Type.IPv4));

        // Parse the Path Attribute section.
        int pathAttributeLength = stream.readUnsignedShort();
        counter = stream.available();

        while(pathAttributeLength > counter - stream.available())
        {
            PathAttribute a = PathAttribute.fromStream(stream, settings);
            pathattributes.put(a.getClass().getSimpleName().toUpperCase(), a);
        }

        // Calculate the remaining size to enable us to parse the NLRI section correctly.
        int NLRI_size = header.getLength() - MIN_UPDATE_SIZE - withdrawnRoutesLength - pathAttributeLength;
        counter = stream.available();
        while(NLRI_size > counter - stream.available())
            nlri.add(new Prefix(stream, Prefix.Type.IPv4));
    }

    public void print()
    {
        System.out.println("BGPUpdate::WithdrawnRoutes");
        for(network.entities.Prefix prefix : withdrawnRoutes)
            prefix.print();

        System.out.println("BGPUpdate::PathAttributes");
        for(Map.Entry<String, PathAttribute> attribute : pathattributes.entrySet())
            attribute.getValue().print();

        System.out.println("BGPUpdate::NLRI");
        for(Prefix prefix : nlri)
            prefix.print();
    }
}
