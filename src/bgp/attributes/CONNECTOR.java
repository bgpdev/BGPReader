package bgp.attributes;

import bgp.AttributeFlags;
import bgp.PathAttribute;

import java.io.DataInputStream;
import java.io.IOException;

public class CONNECTOR extends PathAttribute
{
    public CONNECTOR(AttributeFlags flags, DataInputStream stream) throws IOException
    {
        super(flags);

        int length;
        if(flags.isExtended())
            length = stream.readUnsignedShort();
        else
            length = stream.readUnsignedByte();

        // TODO: Currently the bytes are not being used at all.
        stream.read(new byte[length]);
    }

    @Override
    public void print()
    {
        System.out.println("BGPUpdate::PathAttribute::Connector (Currently not implemented)");
        flags.print();
    }
}
