package bgp.attributes;

import bgp.AttributeFlags;
import bgp.BGPReader;
import bgp.PathAttribute;

import java.io.DataInputStream;
import java.io.IOException;
import java.util.ArrayList;

public class AS4_PATH extends PathAttribute
{
    public final ArrayList<AS_PATH.Segment> path = new ArrayList<>();

    public AS4_PATH(AttributeFlags flags, DataInputStream stream, BGPReader.Settings settings) throws Exception
    {
        super(flags);

        int length;
        if(flags.isExtended())
            length = stream.readUnsignedShort();
        else
            length = stream.readUnsignedByte();

        int counter = stream.available();

        boolean old = settings.EXTENDED_AS4;
        settings.EXTENDED_AS4 = true;

        while(length > counter - stream.available())
            path.add(new AS_PATH.Segment(stream, settings));

        settings.EXTENDED_AS4 = old;
    }

    @Override
    public void print()
    {
        for(AS_PATH.Segment segment : path)
            segment.print();

        flags.print();
    }
}
