package bgp.attributes;

import bgp.AttributeFlags;
import bgp.PathAttribute;
import network.entities.Prefix;

import java.io.DataInputStream;
import java.io.IOException;
import java.util.ArrayList;

public class MP_UNREACH_NLRI extends PathAttribute
{
    public short AFI;
    public byte SAFI;
    public ArrayList<Prefix> nlri = new ArrayList<>();

    public MP_UNREACH_NLRI(AttributeFlags flags, DataInputStream stream) throws IOException
    {
        super(flags);

        int length;
        if(flags.isExtended())
            length = stream.readUnsignedShort();
        else
            length = stream.readUnsignedByte();

        AFI = stream.readShort();
        SAFI = stream.readByte();

        Prefix.Type type = Prefix.Type.IPv4;
        if(AFI == 0x02)
            type = Prefix.Type.IPv6;

        int counter = stream.available();
        while(length - 3 > counter - stream.available())
            nlri.add(new Prefix(stream, type));
    }

    @Override
    public void print()
    {
        System.out.println("BGPUpdate::PathAttribute::MP_UNREACH_NLRI::AFI: " + Short.toUnsignedInt(AFI));
        System.out.println("BGPUpdate::PathAttribute::MP_UNREACH_NLRI::SAFI: " + Byte.toUnsignedInt(SAFI));

        for(Prefix prefix : nlri)
            prefix.print();

        flags.print();
    }
}
