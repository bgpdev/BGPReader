package bgp.attributes;

import bgp.AttributeFlags;
import bgp.BGPReader;
import bgp.PathAttribute;

import java.io.DataInputStream;
import java.net.Inet4Address;
import java.net.InetAddress;

public class AS4_AGGREGATOR extends PathAttribute
{
    public final int AS;
    public final Inet4Address ip;

    public AS4_AGGREGATOR(AttributeFlags flags, DataInputStream stream, BGPReader.Settings settings) throws Exception
    {
        super(flags);

        // Read the Length field
        stream.readUnsignedByte();

        AS = stream.readInt();

        byte[] buffer = new byte[4];
        stream.readFully(buffer);
        ip = (Inet4Address)InetAddress.getByAddress(buffer);
    }

    @Override
    public void print()
    {
        System.out.println("BGPUpdate::PathAttribute::AS4_AGGREGATOR::AS " + Integer.toUnsignedLong(AS));
        System.out.println("BGPUpdate::PathAttribute::AS4_AGGREGATOR::IP " + ip.toString());
        flags.print();
    }
}
