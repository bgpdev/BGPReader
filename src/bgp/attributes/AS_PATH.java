package bgp.attributes;

import bgp.AttributeFlags;
import bgp.BGPReader;
import bgp.PathAttribute;

import java.io.DataInputStream;
import java.io.IOException;
import java.util.ArrayList;

public class AS_PATH extends PathAttribute
{
    public ArrayList<Segment> path = new ArrayList<>();

    public enum Type
    {
        AS_SET,
        AS_SEQUENCE
    };

    public static class Segment
    {
        public final Type type;
        public final ArrayList<Integer> ASNs = new ArrayList<>();

        public Segment(DataInputStream stream, BGPReader.Settings settings) throws Exception
        {
            byte type = stream.readByte();
            if(type == 0x01)
                this.type = Type.AS_SET;
            else if(type == 0x02)
                this.type = Type.AS_SEQUENCE;
            else
                throw new Exception("Unrecognized Segment type: " + type);

            int length = stream.readUnsignedByte();
            for(int i = 0; i < length; i++)
            {
                if(settings.EXTENDED_AS4)
                    ASNs.add(stream.readInt());
                else
                    ASNs.add(stream.readUnsignedShort());
            }
        }

        public void print()
        {
            System.out.println("BGPUpdate::PathAttribute::AS_PATH::Segment:Type: " + type.toString());
            System.out.print("BGPUpdate::PathAttribute::AS_PATH::Segment:ASN: [ ");
            for(Integer AS : ASNs)
                System.out.print(Integer.toUnsignedLong(AS) + " ");
            System.out.println("]");
        }
    }

    public AS_PATH(AS4_PATH as4_path)
    {
        super(as4_path.flags);
        this.path = as4_path.path;
    }

    public AS_PATH(AttributeFlags flags, DataInputStream stream, BGPReader.Settings settings) throws Exception
    {
        super(flags);

        int length;
        if(flags.isExtended())
            length = stream.readUnsignedShort();
        else
            length = stream.readUnsignedByte();


        int counter = stream.available();
        while(length > counter - stream.available())
        {
            path.add(new Segment(stream, settings));
        }
    }

    @Override
    public void print()
    {
        for(Segment segment : path)
            segment.print();

        flags.print();
    }
}
