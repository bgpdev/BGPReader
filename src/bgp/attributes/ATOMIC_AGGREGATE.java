package bgp.attributes;

import bgp.AttributeFlags;
import bgp.PathAttribute;

import java.io.DataInputStream;
import java.io.IOException;

public class ATOMIC_AGGREGATE extends PathAttribute
{
    public ATOMIC_AGGREGATE(AttributeFlags flags, DataInputStream stream) throws IOException
    {
        super(flags);

        // Read the Length byte.
        stream.readUnsignedByte();
    }

    @Override
    public void print()
    {
        System.out.println("BGPUpdate::PathAttribute::ATOMIC_AGGREGATE");
        flags.print();
    }
}
