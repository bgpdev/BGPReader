package bgp.attributes;

import bgp.AttributeFlags;
import bgp.PathAttribute;

import java.io.DataInputStream;
import java.io.IOException;

public class MULTI_EXIT_DISC extends PathAttribute
{
    public final int value;

    public MULTI_EXIT_DISC(AttributeFlags flags, DataInputStream stream) throws IOException
    {
        super(flags);

        // Read the Length Field
        stream.readUnsignedByte();

        value = stream.readInt();
    }

    @Override
    public void print()
    {
        System.out.println("BGPUpdate::PathAttribute::MULTI_EXIT_DISC: " + Integer.toUnsignedLong(value));
        flags.print();
    }
}
