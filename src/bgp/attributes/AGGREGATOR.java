package bgp.attributes;

import bgp.AttributeFlags;
import bgp.BGPReader;
import bgp.PathAttribute;

import java.io.DataInputStream;
import java.io.IOException;
import java.net.Inet4Address;
import java.net.InetAddress;

public class AGGREGATOR extends PathAttribute
{
    public final int AS;
    public final Inet4Address ip;

    public AGGREGATOR(AS4_AGGREGATOR aggregator)
    {
        super(aggregator.flags);
        this.AS = aggregator.AS;
        this.ip = aggregator.ip;
    }

    public AGGREGATOR(AttributeFlags flags, DataInputStream stream, BGPReader.Settings settings) throws Exception
    {
        super(flags);

        // Read the Length field
        int length = stream.readUnsignedByte();

        if(length == 8)
            AS = stream.readInt();
        else if(length == 6)
            AS = stream.readShort();
        else
            throw new Exception("Unrecognized Aggregator length: " + length);

        byte[] buffer = new byte[4];
        stream.readFully(buffer);
        ip = (Inet4Address)InetAddress.getByAddress(buffer);
    }

    @Override
    public void print()
    {
        System.out.println("BGPUpdate::PathAttribute::AGGREGATOR::AS " + Integer.toUnsignedLong(AS));
        System.out.println("BGPUpdate::PathAttribute::AGGREGATOR::IP " + ip.toString());
        flags.print();
    }
}
