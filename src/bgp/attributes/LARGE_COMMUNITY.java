package bgp.attributes;

import bgp.AttributeFlags;
import bgp.PathAttribute;

import java.io.DataInputStream;
import java.io.IOException;
import java.util.ArrayList;

public class LARGE_COMMUNITY extends PathAttribute
{
    public final ArrayList<Community> community = new ArrayList<>();

    public class Community
    {
        public final int globalAdministrator;
        public final int data1;
        public final int data2;

        public Community(DataInputStream stream) throws IOException
        {
            globalAdministrator = stream.readInt();
            data1 = stream.readInt();
            data2 = stream.readInt();
        }
    }

    public LARGE_COMMUNITY(AttributeFlags flags, DataInputStream stream) throws IOException
    {
        super(flags);

        int length;
        if(flags.isExtended())
            length = stream.readUnsignedShort();
        else
            length = stream.readUnsignedByte();

        for(int i = 0; i < length / 12; i++)
            community.add(new Community(stream));
    }

    @Override
    public void print()
    {
        for(Community x : community)
        {
            System.out.println("BGPUpdate::PathAttribute::LARGE_COMMUNITY::Administrator: " + Integer.toUnsignedLong(x.globalAdministrator));
            System.out.println("BGPUpdate::PathAttribute::LARGE_COMMUNITY::Data: " + x.data1 + "|" + x.data2);
        }

        flags.print();
    }
}
