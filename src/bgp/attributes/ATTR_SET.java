package bgp.attributes;

import bgp.AttributeFlags;
import bgp.BGPReader;
import bgp.PathAttribute;

import java.io.DataInputStream;

public class ATTR_SET extends PathAttribute
{
    public final long origin;
    public final byte[] attributes;

    public ATTR_SET(AttributeFlags flags, DataInputStream stream) throws Exception
    {
        super(flags);

        int length;
        if(flags.isExtended())
            length = stream.readUnsignedShort();
        else
            length = stream.readUnsignedByte();

        origin = Integer.toUnsignedLong(stream.readInt());

        attributes = new byte[length - 4];
        stream.readFully(attributes);
    }

    @Override
    public void print()
    {
        System.out.println("BGPUpdate::PathAttribute::ATTR_SET::Origin " + origin);
        System.out.println("BGPUpdate::PathAttribute::ATTR_SET::Attributes " + attributes.length);
    }
}
