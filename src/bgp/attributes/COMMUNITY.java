package bgp.attributes;

import bgp.AttributeFlags;
import bgp.PathAttribute;

import java.io.DataInputStream;
import java.io.IOException;
import java.util.ArrayList;

public class COMMUNITY extends PathAttribute
{
    public final ArrayList<Community> community = new ArrayList<>();

    public class Community
    {
        public final short AS;
        public final short value;

        public Community(DataInputStream stream) throws IOException
        {
            AS = stream.readShort();
            value = stream.readShort();
        }
    }

    public COMMUNITY(AttributeFlags flags, DataInputStream stream) throws IOException
    {
        super(flags);

        int length;
        if(flags.isExtended())
            length = stream.readUnsignedShort();
        else
            length = stream.readUnsignedByte();

        for(int i = 0; i < length / 4; i++)
            community.add(new Community(stream));
    }

    @Override
    public void print()
    {
        for(Community x : community)
            System.out.println("BGPUpdate::PathAttribute::COMMUNITY: " + Short.toUnsignedInt(x.AS) + "|" + x.value);

        flags.print();
    }
}
