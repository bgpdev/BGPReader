package bgp.attributes;

import bgp.AttributeFlags;
import bgp.BGPReader;
import bgp.PathAttribute;
import network.entities.Prefix;

import java.io.DataInputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.util.ArrayList;

public class MP_REACH_NLRI extends PathAttribute
{
    public short AFI;
    public byte SAFI;
    public InetAddress NEXT_HOP;
    public ArrayList<Prefix> nlri = new ArrayList<>();

    public MP_REACH_NLRI(AttributeFlags flags, DataInputStream stream, BGPReader.Settings settings) throws IOException
    {
        super(flags);

        int length;
        if(flags.isExtended())
            length = stream.readUnsignedShort();
        else
            length = stream.readUnsignedByte();

        /*
         * The MRT file format handles this field differently.
         * See: https://tools.ietf.org/html/rfc6396, Section 4.3.4
         */

        if(settings.MRTMode)
        {
            byte[] buffer = new byte[stream.readUnsignedByte()];
            stream.readFully(buffer);
            if(buffer.length == 4 || buffer.length == 16)
                NEXT_HOP = InetAddress.getByAddress(buffer);
            return;
        }

        AFI = stream.readShort();
        SAFI = stream.readByte();

        Prefix.Type type = Prefix.Type.IPv4;
        if(AFI == 0x02)
            type = Prefix.Type.IPv6;


        int NH_length = stream.readByte();
        byte[] buffer = new byte[NH_length];
        stream.readFully(buffer);
        if(buffer.length == 4 || buffer.length == 16)
            NEXT_HOP = InetAddress.getByAddress(buffer);

        // Read the reserved octet
        stream.readByte();

        int counter = stream.available();
        while(length - (5 + NH_length) > counter - stream.available())
            nlri.add(new Prefix(stream, type));
    }

    @Override
    public void print()
    {
        System.out.println("BGPUpdate::PathAttribute::MP_REACH_NLRI::AFI: " + Short.toUnsignedInt(AFI));
        System.out.println("BGPUpdate::PathAttribute::MP_REACH_NLRI::SAFI: " + Byte.toUnsignedInt(SAFI));

        if(NEXT_HOP != null)
            System.out.println("BGPUpdate::PathAttribute::MP_REACH_NLRI::NEXT_HOP: " + NEXT_HOP.toString());

        for(Prefix prefix : nlri)
            prefix.print();

        flags.print();
    }
}
