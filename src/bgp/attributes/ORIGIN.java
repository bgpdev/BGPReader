package bgp.attributes;

import bgp.AttributeFlags;
import bgp.PathAttribute;

import java.io.DataInputStream;
import java.io.IOException;

public class ORIGIN extends PathAttribute
{
    public final byte value;

    public ORIGIN(AttributeFlags flags, DataInputStream stream) throws IOException
    {
        super(flags);

        // Read the length field.
        stream.readUnsignedByte();

        value = stream.readByte();
    }

    @Override
    public void print()
    {
        switch(Byte.toUnsignedInt(value))
        {
            case 0:
                System.out.println("BGPUpdate::PathAttribute::ORIGIN: IGP");
                break;
            case 1:
                System.out.println("BGPUpdate::PathAttribute::ORIGIN: EGP");
                break;
            case 2:
                System.out.println("BGPUpdate::PathAttribute::ORIGIN: INCOMPLETE");
                break;
        }
        flags.print();
    }
}
