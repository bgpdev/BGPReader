package bgp.attributes;

import bgp.AttributeFlags;
import bgp.PathAttribute;

import java.io.DataInputStream;
import java.io.IOException;
import java.util.ArrayList;

public class EXTENDED_COMMUNITY extends PathAttribute
{
    public final ArrayList<Community> community = new ArrayList<>();

    public class Community
    {
        public final long data;

        public Community(DataInputStream stream) throws IOException
        {
            data = stream.readLong();
        }
    }

    public EXTENDED_COMMUNITY(AttributeFlags flags, DataInputStream stream) throws IOException
    {
        super(flags);

        int length;
        if(flags.isExtended())
            length = stream.readUnsignedShort();
        else
            length = stream.readUnsignedByte();

        for(int i = 0; i < length / 8; i++)
            community.add(new Community(stream));
    }

    @Override
    public void print()
    {
        for(Community x : community)
            System.out.println("BGPUpdate::PathAttribute::EXTENDED_COMMUNITY: " + x.data);

        flags.print();
    }
}
