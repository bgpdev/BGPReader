package bgp.attributes;

import bgp.AttributeFlags;
import bgp.PathAttribute;

import java.io.DataInputStream;

public class AS_PATHLIMIT extends PathAttribute
{
    public final int max;
    public final long AS;

    public AS_PATHLIMIT(AttributeFlags flags, DataInputStream stream) throws Exception
    {
        super(flags);

        byte length = stream.readByte();
        max = stream.readUnsignedByte();
        AS = Integer.toUnsignedLong(stream.readInt());
    }

    @Override
    public void print()
    {
        System.out.println("BGPUpdate::PathAttribute::AS_PATHLIMIT::Max " + max);
        System.out.println("BGPUpdate::PathAttribute::AS_PATHLIMIT::AS " + AS);
    }
}
