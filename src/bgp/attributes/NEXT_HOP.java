package bgp.attributes;

import bgp.AttributeFlags;
import bgp.PathAttribute;

import java.io.DataInputStream;
import java.io.IOException;
import java.net.Inet4Address;
import java.net.InetAddress;

public class NEXT_HOP extends PathAttribute
{
    public final InetAddress ip;

    public NEXT_HOP(AttributeFlags flags, InetAddress ip)
    {
        super(flags);
        this.ip = ip;
    }

    public NEXT_HOP(AttributeFlags flags, DataInputStream stream) throws IOException
    {
        super(flags);

        // Read the Length field.
        stream.readUnsignedByte();

        byte[] buffer = new byte[4];
        stream.readFully(buffer);
        ip = InetAddress.getByAddress(buffer);
    }

    @Override
    public void print()
    {
        System.out.println("BGPUpdate::PathAttribute::NEXT_HOP: " + ip.toString());
        flags.print();
    }
}
