package bgp;

import java.io.DataInputStream;

public class BGPHeader
{
    private final short length;
    private final byte type;

    public BGPHeader(DataInputStream stream) throws Exception
    {
        // Read the marker and discard the results.
        stream.readFully(new byte[16]);

        this.length = stream.readShort();
        this.type = stream.readByte();
    }

    public int getLength()
    {
        return Short.toUnsignedInt(length);
    }

    public int getType()
    {
        return Byte.toUnsignedInt(type);
    }

    public void print()
    {
        System.out.println("BGPHeader::Length: " + Short.toUnsignedInt(length));
        System.out.println("BGPHeader::Type: " + Byte.toUnsignedInt(type));
    }
}
