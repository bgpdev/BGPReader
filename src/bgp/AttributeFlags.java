package bgp;

public class AttributeFlags
{
    private final int flags;

    public AttributeFlags(byte flags)
    {
        this.flags = Byte.toUnsignedInt(flags);
    }

    public boolean isOptional()
    {
        return (flags & (1 << 7)) != 0;
    }

    public boolean isTransitive()
    {
        return (flags & (1 << 6)) != 0;
    }

    public boolean isPartial()
    {
        return (flags & (1 << 5)) != 0;
    }

    public boolean isExtended()
    {
        return (flags & (1 << 4)) != 0;
    }

    public void print()
    {
        System.out.println("BGPUpdate::PathAttribute::Flags::Optional: " + isOptional());
        System.out.println("BGPUpdate::PathAttribute::Flags::Transitive: " + isTransitive());
        System.out.println("BGPUpdate::PathAttribute::Flags::Partial: " + isPartial());
        System.out.println("BGPUpdate::PathAttribute::Flags::Extended: " + isExtended());
    }
}
