package bgp.utility;

import bgp.BGPReader;
import bgp.BGPUpdate;
import bgp.PathAttribute;
import bgp.attributes.*;
import utility.Pair;
import utility.Triple;

import java.io.DataInputStream;
import java.math.BigInteger;
import java.util.*;

public class BGPUtility
{
    public static AS_PATH getAS_PATH(BGPUpdate update)
    {
        return (AS_PATH)update.pathattributes.get("AS_PATH");
    }

    public static AS_PATH getAS_PATH(DataInputStream attributes) throws Exception
    {
        BGPReader.Settings settings = new BGPReader.Settings();
        settings.EXTENDED_AS4 = true;
        settings.MRTMode = false;

        while(attributes.available() > 0)
        {
            PathAttribute x = PathAttribute.fromStream(attributes, settings);
            if (x instanceof AS_PATH)
                return (AS_PATH) x;
            if(x instanceof AS4_PATH)
                return new AS_PATH((AS4_PATH)x);
        }

        throw new Exception("No AS_PATH attribute present.");
    }

    public static AS_PATH getAS_PATH(ArrayList<PathAttribute> attributes) throws Exception
    {
        for(PathAttribute x : attributes)
            if (x instanceof AS_PATH)
                return (AS_PATH) x;

        throw new Exception("No AS_PATH attribute present.");
    }

    public static List<Long> getPathList(BGPUpdate update)
    {
        List<Long> list = new ArrayList<>();
        if(!update.pathattributes.containsKey("AS_PATH"))
            return list;

        AS_PATH path = (AS_PATH)update.pathattributes.get("AS_PATH");
        for(AS_PATH.Segment segment : path.path)
        {
            if(segment.type == AS_PATH.Type.AS_SEQUENCE)
                for(Integer i : segment.ASNs)
                    list.add(Integer.toUnsignedLong(i));
        }

        return list;
    }

    public static long getOriginatingAS(AS_PATH path)
    {
        AS_PATH.Segment segment = path.path.get(path.path.size() - 1);

        assert (segment.type == AS_PATH.Type.AS_SEQUENCE);
        return Integer.toUnsignedLong(segment.ASNs.get(segment.ASNs.size() - 1));
    }


    public static HashSet<Integer> getPathSet(BGPUpdate update)
    {
        HashSet<Integer> set = new HashSet<>();

        if(!update.pathattributes.containsKey("AS_PATH"))
            return set;

        AS_PATH path = (AS_PATH)update.pathattributes.get("AS_PATH");
        for(AS_PATH.Segment segment : path.path)
        {
            set.addAll(segment.ASNs);
        }

        return set;
    }

    public static ArrayList<Long> getReversePath(BGPUpdate update)
    {
        ArrayList<Long> list = new ArrayList<>();
        if(!update.pathattributes.containsKey("AS_PATH"))
            return list;

        AS_PATH path = (AS_PATH)update.pathattributes.get("AS_PATH");
        for(AS_PATH.Segment segment : path.path)
            for(Integer i : segment.ASNs)
                list.add(Integer.toUnsignedLong(i));

        Collections.reverse(list);
        return list;
    }

    public static ArrayList<Long> getReversePath(Map<String, PathAttribute> attributes)
    {
        ArrayList<Long> list = new ArrayList<>();
        if(!attributes.containsKey("AS_PATH"))
            return list;

        AS_PATH path = (AS_PATH)attributes.get("AS_PATH");
        for(AS_PATH.Segment segment : path.path)
            for(Integer i : segment.ASNs)
                list.add(Integer.toUnsignedLong(i));

        Collections.reverse(list);
        return list;
    }

    public static ArrayList<Long> getReversePath(DataInputStream attributes) throws Exception
    {
        BGPReader.Settings settings = new BGPReader.Settings();
        settings.EXTENDED_AS4 = true;
        settings.MRTMode = false;

        ArrayList<Long> list = new ArrayList<>();
        while (attributes.available() > 0)
        {
            PathAttribute x = PathAttribute.fromStream(attributes, settings);
            if (x instanceof AS_PATH)
                for (AS_PATH.Segment segment : ((AS_PATH) x).path)
                    for (Integer i : segment.ASNs)
                        list.add(Integer.toUnsignedLong(i));
        }


        Collections.reverse(list);
        return list;
    }

    /**
     * Returns all Triples (AS1, AS2, AS3) from an AS_PATH.
     * @return Returns a Collection of Triples that the AS_PATH consists of.
     */
    public static Collection<Triple<Integer, Integer, Integer>> getTriples(AS_PATH path)
    {
        Set<Triple<Integer, Integer, Integer>> set = new HashSet<>();

        Integer previous = null;
        for(AS_PATH.Segment segment : path.path)
        {
            if(segment.type == AS_PATH.Type.AS_SEQUENCE)
            {
                for (int i = 0; i < segment.ASNs.size(); i++)
                {
                    if(previous != null && (segment.ASNs.size() - 1 != i))
                        set.add(new Triple<>(previous, segment.ASNs.get(i), segment.ASNs.get(i + 1)));

                    previous = segment.ASNs.get(i);
                }
            }

            if(segment.type == AS_PATH.Type.AS_SET)
                previous = null;
        }

        return set;
    }

    /**
     * Returns all the relationships within a apth
     * @return Returns a Collection of Triples that the AS_PATH consists of.
     */
    public static List<Pair<Long, Long>> getPairs(AS_PATH path)
    {
        ArrayList<Pair<Long, Long>> list = new ArrayList<>();

        Long previous = null;
        for(AS_PATH.Segment segment : path.path)
        {
            if(segment.type == AS_PATH.Type.AS_SEQUENCE)
            {
                for (Integer AS : segment.ASNs)
                {
                    if(previous != null)
                        list.add(new Pair<>(Integer.toUnsignedLong(AS), previous));

                    previous = Integer.toUnsignedLong(AS);
                }
            }

            // TODO: We cannot know for sure that an AS_SET are direct links.
            if(segment.type == AS_PATH.Type.AS_SET)
                previous = null;
        }

        return list;
    }

    /**
     * Returns a AS_PATH as string representation.
     * @param path The AS_PATH to be converted to String.
     * @return The AS_PATH converted to String.
     */
    public static String toString(AS_PATH path)
    {
        StringBuilder string = new StringBuilder();
        for(AS_PATH.Segment segment : path.path)
        {
            if(segment.type == AS_PATH.Type.AS_SEQUENCE)
                for (int i = 0; i < segment.ASNs.size(); i++)
                    string.append(segment.ASNs.get(i)).append(", ");

            if(segment.type == AS_PATH.Type.AS_SET)
            {
                string.append("( ");
                for (int i = 0; i < segment.ASNs.size(); i++)
                    string.append(segment.ASNs.get(i)).append(", ");
                string.append("), ");
            }
        }

        return string.toString();
    }

    public static void normalize(BGPUpdate update)
    {
        HashMap<String, PathAttribute> left = new HashMap<>();
        for(Iterator<String> it = update.pathattributes.keySet().iterator(); it.hasNext();)
        {
            String key = it.next();
            switch(key)
            {
                case "AS4_AGGREGATOR":
                {
                    AS4_AGGREGATOR aggregator = (AS4_AGGREGATOR) update.pathattributes.get(key);
                    it.remove();
                    left.put("AS_AGGREGATOR", new AGGREGATOR(aggregator));
                    break;
                }
                case "AS4_PATH":
                {
                    AS4_PATH path = (AS4_PATH) update.pathattributes.get(key);
                    it.remove();
                    left.put("AS_PATH", new AS_PATH(path));
                    break;
                }
                case "MP_REACH_NLRI":
                {
                    MP_REACH_NLRI mp = (MP_REACH_NLRI) update.pathattributes.get(key);
                    left.put("NEXT_HOP", new NEXT_HOP(mp.flags, mp.NEXT_HOP));
                    update.nlri.addAll(mp.nlri);
                    it.remove();
                    break;
                }
                case "MP_UNREACH_NLRI":
                {
                    MP_UNREACH_NLRI mp = (MP_UNREACH_NLRI) update.pathattributes.get(key);
                    update.withdrawnRoutes.addAll(mp.nlri);
                    it.remove();
                    break;
                }
            }
        }

        for(Map.Entry<String, PathAttribute> x : left.entrySet())
            update.pathattributes.put(x.getKey(), x.getValue());
    }

    public static String[] getRRCs()
    {
        return new String[]{"rrc00",
                "rrc01", "rrc03", "rrc04","rrc05","rrc06","rrc07","rrc10","rrc11","rrc12","rrc13",
                "rrc14","rrc15","rrc16","rrc18","rrc19","rrc20","rrc21","rrc22","rrc23"};
    }

    public static void sanitize(AS_PATH path)
    {
        /* -----------------------------
         * Check for duplicates
         * -----------------------------*/
        HashMap<Integer, BigInteger> duplicates = new HashMap<>();

        for(AS_PATH.Segment segment : path.path)
        {
            ListIterator<Integer> iterator = segment.ASNs.listIterator();
            while(iterator.hasNext())
            {
                Integer next = iterator.next();

                // Remove all IXP ASN numbers.
                if(isIXP(next))
                    iterator.remove();

                // Register duplicates and their span.
                else if(duplicates.containsKey(next))
                    duplicates.put(next, new BigInteger(next.toString()));
                else
                    duplicates.put(next, null);
            }
        }

        boolean filter = false;
        for(AS_PATH.Segment segment : path.path)
        {
            if(segment.type == AS_PATH.Type.AS_SEQUENCE)
            {
                ListIterator<Integer> iterator = segment.ASNs.listIterator();
                while (iterator.hasNext())
                {
                    Integer next = iterator.next();

                    // If a duplicate has been seen but it is not the end node, start filtering.
                    if(duplicates.get(next) != null)
                    {
                        if(duplicates.get(next) != new BigInteger(next.toString()))
                        {
                            iterator.remove();
                            filter = true;
                        }
                        else
                            filter = false;
                    }
                    else if(filter)
                        iterator.remove();
                }
            }

            if(segment.type == AS_PATH.Type.AS_SET)
                for (Integer next : segment.ASNs)
                    if (duplicates.get(next) == new BigInteger(next.toString()))
                        filter = false;
        }
    }

    public static HashSet<Integer> IXPs = new HashSet<>(Arrays.asList(43531, 36932, 24029));

    public static boolean isIXP(Integer ASN)
    {
        return IXPs.contains(ASN);
    }
}
